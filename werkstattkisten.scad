// werkstattkisten.scad
// Autor: André Holstein, Magdeburg
// Lizenz: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


$fn = 50;

breite = 400;
tiefe  = 300;
hoehe  = 240; // z.B. 120 (flach) oder 240 (hoch)
ms = 8; // Materialstärke

nz = 11;    // Anzahl der (Finger)zinken (für ungerade Werte optimiert: z.B. 7 oder 11)
nzb = 7;   // Anzahl der (Finger)zinken am Boden

breite_griff = 80;
breite_fuss = 50;

fd = 2.5;   // Fräserdurchmesser --> damit die Ecken etwas "runder" werden

br6 = 20;   // Breite des Sechseckes (von Seite zu Seite)
sl6 = (br6-fd)/sqrt(3)+fd/2;


module square_cnc(x,y,r){   // "tiefere" Ecken wegen Fräserdurchmesser
    square([x,y]);
    translate([r*sin(45),r*cos(45)]) circle(r);
    translate([r*sin(45),y-r*cos(45)]) circle(r);
    translate([x-r*sin(45),r*cos(45)]) circle(r);
    translate([x-r*sin(45),y-r*cos(45)]) circle(r);
}

module sechseck(){  // mit runden Ecken
    hull(){
        for (i=[1:6]){
            rotate([0,0,i*60]) translate([sl6-fd/2,0]) circle(r=fd/2);
        }
    }
}

module fuss(breite){
    difference(){
        union(){
            translate([0,br6/2-fd/2]) square([breite,br6]);
            hull(){
                square([1,br6/2]);
                translate([breite_fuss,br6/2]) sechseck();
            }
            hull(){
                translate([breite-1,0]) square([1,br6/2]);
                translate([breite-breite_fuss,br6/2]) sechseck();
            }
        }
        hull(){
            translate([breite_fuss+sqrt(3)*br6/2,0]) sechseck();
            translate([breite-breite_fuss-sqrt(3)*br6/2,0]) sechseck();
        }
    }
}

module lange_seite(){
    difference(){
        union(){
            difference(){
                translate([0,br6/2]) square([breite,hoehe]);
                translate([0,hoehe]) fuss(breite);
                hull(){
                    translate([(breite-breite_griff)/2,hoehe-br6/2]) sechseck();
                    translate([(breite+breite_griff)/2,hoehe-br6/2]) sechseck();
                }
            }
            fuss(breite);
        }
        for (i=[1:2:nzb]){  // Fingerzinken am Boden
            translate([i*breite/nzb,br6/2+ms]) square_cnc(breite/nzb,ms,fd/2);
        }
        for (i=[1:2:nz-1]){   // Fingerzinken an den Seiten
            translate([-fd,i*hoehe/nz])       square_cnc(ms+fd,hoehe/nz,fd/2);
            translate([breite-ms,i*hoehe/nz]) square_cnc(ms+fd,hoehe/nz,fd/2);
        }
    }
}

module kurze_seite(){
    difference(){
        union(){
            difference(){
                translate([0,br6/2]) square([tiefe,hoehe]);
                translate([0,hoehe]) fuss(tiefe);
                hull(){
                    translate([(tiefe-breite_griff)/2,hoehe-br6/2]) sechseck();
                    translate([(tiefe+breite_griff)/2,hoehe-br6/2]) sechseck();
                }
            }
            fuss(tiefe);
        }
        for (i=[1:2:nzb]){  // Fingerzinken am Boden
            translate([i*tiefe/nzb,br6/2+ms]) square_cnc(tiefe/nzb,ms,fd/2);
        }
        for (i=[0:2:nz]){   // Fingerzinken an den Seiten
            if (i==0){
                translate([-fd,-fd])      square_cnc(ms+fd,hoehe/nz+fd,fd/2);
                translate([tiefe-ms,-fd]) square_cnc(ms+fd,hoehe/nz+fd,fd/2);
            } else {
                if (i==nz-1) {
                    translate([-fd,i*hoehe/nz])      square_cnc(ms+fd,hoehe/nz+fd,fd/2);
                    translate([tiefe-ms,i*hoehe/nz]) square_cnc(ms+fd,hoehe/nz+fd,fd/2);
                } else {
                    translate([-fd,i*hoehe/nz])      square_cnc(ms+fd,hoehe/nz,fd/2);
                    translate([tiefe-ms,i*hoehe/nz]) square_cnc(ms+fd,hoehe/nz,fd/2);
                }
            }
        }
    }
}

module boden(){
    difference(){
        square([breite,tiefe]);
        for (i=[0:2:nzb]){  
            translate([i*breite/nzb,-fd]) square_cnc(breite/nzb,ms+fd,fd/2);
            translate([i*breite/nzb,tiefe-ms]) square_cnc(breite/nzb,ms+fd,fd/2);
            translate([-fd,i*tiefe/nzb]) square_cnc(ms+fd,tiefe/nzb,fd/2);
            translate([breite-ms,i*tiefe/nzb]) square_cnc(ms+fd,tiefe/nzb,fd/2);
        }
    }   
}



translate([0,0]) lange_seite();
translate([breite+5,0]) kurze_seite();

translate([0,hoehe+5]) lange_seite();
translate([breite+5,hoehe+5]) kurze_seite();

translate([breite+2*tiefe+10,0]) rotate([0,0,90]) boden();


