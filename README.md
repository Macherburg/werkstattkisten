# werkstattkisten

## Lizenz
* Autor: André Holstein, Magdeburg
* Lizenz: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


## Anleitung
DXF und SVG Dateien sind schon für Kisten mit einer Grundfläche von 300mm x 400mm vorbereitet. Auswahl zwischen 120mm (flach) oder 240mm (hoch) und zwischen 6mm Materialstärke oder 8mm Materialstärke.
Für das Ausfräsen der Kisten empfehlen wir einen Fräserdurchmesser von ca. 2,5mm (zumindest sind die Dateien dafür optimmiert).

Für indivudelle Maße kann die SCAD Datei mit https://www.openscad.org/ angepasst und individuell gerendert werden.



